/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    NodeBlock,
    Slots,
    conditions,
    each,
    editor,
    pgettext,
    slots,
    tripetto,
} from "@tripetto/builder";
import { URLCondition } from "./condition";
import { TMode } from "../runner/mode";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_DEFINED from "../../assets/icon-defined.svg";
import ICON_UNDEFINED from "../../assets/icon-undefined.svg";

@tripetto({
    type: "node",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    get label() {
        return pgettext("block:url", "URL");
    },
})
export class URL extends NodeBlock {
    urlSlot!: Slots.String;

    @slots
    defineSlot(): void {
        this.urlSlot = this.slots.static({
            type: Slots.String,
            reference: "url",
            label: URL.label,
            exchange: ["required", "alias", "exportable"],
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name();
        this.editor.description();
        this.editor.placeholder();
        this.editor.explanation();

        this.editor.groups.options();
        this.editor.required(this.urlSlot);
        this.editor.visibility();
        this.editor.alias(this.urlSlot);
        this.editor.exportable(this.urlSlot);
    }

    @conditions
    defineCondition(): void {
        each(
            [
                {
                    mode: "defined",
                    label: pgettext("block:url", "URL is not empty"),
                    icon: ICON_DEFINED,
                },
                {
                    mode: "undefined",
                    label: pgettext("block:url", "URL is empty"),
                    icon: ICON_UNDEFINED,
                },
            ],
            (condition: { mode: TMode; label: string; icon: string }) => {
                this.conditions.template({
                    condition: URLCondition,
                    label: condition.label,
                    icon: condition.icon,
                    burst: true,
                    props: {
                        slot: this.urlSlot,
                        mode: condition.mode,
                    },
                });
            }
        );
    }
}

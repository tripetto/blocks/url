/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import { ConditionBlock, condition, tripetto } from "@tripetto/runner";
import { TMode } from "./mode";

@tripetto({
    type: "condition",
    legacyBlock: true,
    identifier: PACKAGE_NAME,
})
export class URLCondition extends ConditionBlock<{
    readonly mode: TMode;
}> {
    @condition
    verify(): boolean {
        const urlSlot = this.valueOf<string>();

        if (urlSlot) {
            switch (this.props.mode) {
                case "defined":
                    return urlSlot.string !== "";
                case "undefined":
                    return urlSlot.string === "";
            }
        }

        return false;
    }
}
